<?php
	if(isset($_GET['mob'])){
		 $mob = $_GET['mob'];
	}else{
		 $mob = 0;
	}
?>
<html>
<head>
	<title>Confessions | Main</title>
	<link rel="shortcut icon" href="img/fav_icon.jpg">
	<link rel="stylesheet" type="text/css" href="custom_style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:title" content="Confessions | Main" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://apps.facebook.com/iconfessions/" />
	<meta property="og:image" content="http://iconfessions.herokuapp.com/img/fav_icon_200.png" />
	<meta property="fb:admins" content="XXXX"/>
	<meta property="fb:app_id" content="XXXX"/>
</head>
<body>
	<!--Fb-Script-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=XXXX";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<!--Fb-Script-End------>

	<?php
		if($mob==101){
	?>
		<div class="logo"><a href="index.php?mob=101">iConfessions</a></div>
	<?php }else { ?>
		<div class="logo"><a href="index.php">iConfessions</a></div>
	<?php } ?>

	<div id="welcome_msg" class="welcome_msg">read and write truth !</div>

	<div id="saveInfo" class="saveInfo">
	<?php
		if($mob==101){
	?>	
		<form action="scripts/add_confessions_mongoDB.php?mob=101" id="FRM" onsubmit="return submitInfo()" method="post">
	<?php }else {?>
		<form action="scripts/add_confessions_mongoDB.php" id="FRM" onsubmit="return submitInfo()" method="post">
	<?php }?>

				<label id="confession-head">Confess : <br /><span class="special_info">If you think that you've done something wrong ,or if you are living any secretive life like BATMAN, SUPERMAN and SHAKTIMAN . Just confess it. Read other's confessions and write your opinions.</span></label>
				<br />
				<textarea id="confession" name="confession" class="textarea"></textarea>
				<br />
				<button class="btn" type="submit">Submit</button>
	</form>
	<?php
		if(isset($_GET['response_code'])){
			$response_code=$_GET['response_code'];
		}else{
			$response_code=0;
		}
		
		if($response_code==202){
	?>
	<h1 class="response">
		Confession submitted !!
	</h1>	
	<?php
		}
	?>
	</div>
	<div class="confessionsData" id="confessionsData">
		<div class="info">
			<span class="a">confess freely ,your personal details will not shared. </span>click on any confession id to add or view comments.
		</div>

		<br />
		
		<?php
			include 'scripts/show_all_mongoDB_confessions.php';
		?>

	</div>
	<div class="footer">
		contact <a href="mailto:1001.fb.apps@gmail.com">1001.fb.apps@gmail.com</a> for any support.<br />
		<div id="fblike" class="fb-like" data-href="https://iconfessions.herokuapp.com" data-layout="button" data-action="recommend" data-show-faces="true" data-share="false"></div>
	<br />
	</div>
	<script type="text/javascript" src="js/main_validate.js"></script>
</body>
</html>