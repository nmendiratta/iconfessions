<?php
	if(isset($_GET['id'])){
		$code = $_GET['id'];
	}else{
		$code = 'null';
	}
		
	if(isset($_GET['mob'])){
		$mob = $_GET['mob'];
	}else{
		$mob = 0;
	}
?>


<html>
<head>
	<title>Confessions | Main</title>
	<link rel="shortcut icon" href="img/fav_icon.jpg">
	<link rel="stylesheet" type="text/css" href="custom_style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Post At Confessions" />
	<meta property="og:url" content="https://apps.facebook.com/iconfessions/post.php?id=<?php echo $code;?>" />
	<meta property="og:image" content="http://iconfessions.herokuapp.com/img/fav_icon_200.png" />
	<meta property="og:description" content="Confession No#<?php echo $code ;?>" />
	<meta property="og:site_name" content="Confessions" />
	<meta property="fb:admins" content="XXXX"/>
	<meta property="fb:app_id" content="XXXX"/>
</head>
<body>
	<!--Fb-Script-->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=XXXX";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
		<!--Fb-Script-End------>
	<?php
		if($mob==101){
	?>
		<div class="logo"><a href="index.php?mob=101">iConfessions</a></div>
	<?php }else { ?>
		<div class="logo"><a href="index.php">iConfessions</a></div>
	<?php } ?>

	<div id="welcome_msg" class="welcome_msg">read and write truth !</div>

	<div class="confessionsData" id="confessionsData">

		<?php
			include 'scripts/show_single_mongoDB_confession.php';
			$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			echo "<div class='fb-like' data-href='$actual_link' data-layout='standard' data-action='like' data-show-faces='false' data-share='true'></div>";
		 	echo "<br />";
		 	echo "<br />";
		 	echo "<div class='fb-comments' data-href='$actual_link' data-numposts='10' data-width='800'></div>";		    
		?>

	</div>
	<div class="footer">
		contact <a href="mailto:1001.fb.apps@gmail.com">1001.fb.apps@gmail.com</a> for any support.<br />
		<div id="fblike" class="fb-like" data-href="https://iconfessions.herokuapp.com" data-layout="button" data-action="recommend" data-show-faces="true" data-share="false"></div>
	<br />
	</div>
</body>
<script type="text/javascript" src="js/main_validate.js"></script>
</html>