<?php
 try {
   $user = 'username';
   $pass = 'password';
   $app = 'mongoapp_id';
   $col = 'collection_name';
   echo 'connecting ...';
   $connection = new Mongo('mongodb://username:password@alex.mongohq.com:10018/mongoapp_id');
   echo 'connected';
   $database   = $connection->selectDB($app);
   $collection = $database->selectCollection($col);
  }
  catch(MongoConnectionException $e) {
   die("Failed to connect to database ".$e->getMessage());
  }
?>